# Times for tasks and additional notes

* Personal Notes were taken during each task
* Cloning gitlab and installing intelliJ - 10 mins
* Reading 'No Silver Bullet: Essence and Accident in Software Engineering' - 20 mins
* Reading 'They Write the Right Stuff' - 13 mins
* CHM Live | The History (and the Future) of Software - 1:09:27
* Write ups - 30 mins
* Getting started with Java - 30 mins
* The Money Example - 4 hrs 30 mins + longer because I had to redo it... 


*Start of Personal NOTES*

# No Silver Bullet
- Started 1:19
- Those concerned with fashioning abstract conceptual structures of great complexity should:
- Exploit the mass market to avoid constructing what can be bought
- Use rapid prototyping
- Grow software organically adding more and more function systems as they are run, used, and tested
- Identify and develop the great conceptual designers of the rising generation
- There is no single development that promises improvement in productivity, reliability, and simplicity
- There is no inherently silver bullet
- Conformity: software is complex
- Changeability: software entity is constantly subject to pressures of change
- Invisibility: in restricting and simplifying software, they remain inherently unvisualizable
- Three steps of in software technology that have been most fruitful: High level languages, time-sharing, unified programming environments
- Potential Silver Bullets: Ada and other high-level language advances, OOP, AI, Exoert Systems, Auto Programming, Graphical Programming, Programming verification, Environments and tools, and Workstations
- Buy Verses Build: A purchased piece of software is  costing only about as much as one programmer-year  100k
- The hardest part about building a software system is deciding precisely what to build
- Incremental development-grow, not build, software: teams can grow much more complex entities in four months than they can build
- Great designers: follow good practices, not bad ones
- Done 1:40

# They Write the Right Stuff
- Started: 1:45
- The right stuff is in the software
- The software gives orders throttles engines, keeps track of where the shuttle is, makes minor corrections, and directs the shuttle into orbit
- Software is so powerful and essential it controls human life on the shuttle
- The group's most important creation is not the perfect software they write -- it's the process they invented that writes the perfect software
- The process can be reduced to four simple propositions:
1. The product is only as good as the plan for the product
2. The best teamwork is a healthy rivalry
3. The database is the software base
4. Don't just fix mistakes, fix whatever permitted the mistake in the first place
- Software is getting more and more common and more and more important, but it doesn't seem to be getting more and more reliable
- The sooner you fall behind, the more time you will have to catch up
- Ended: 1:58

# CHM Live | The History (and the Future) of Software
- Software is the invisible thread and hardware is the invisible loom in which we weave computing
- Software is perhaps the most durable of materials - it can move virtual/real mountains, it can move markets, and move the hearts and minds of individuals
- Software is the most creative of mediums - with a blank page you can write anything
- The journey of software - Assembly languages and punch cards to Outsourcing the mind to a smartphone.. lol
- Software is fragile, durable, fungible
- SD is like building a doghouse
- A lot of software is like managing a city
- You want to take software and slowly evolve it over time like paris vs china
- Developing software is like raising a child, shooting a movie
- "the first computer were woman at Harvard"
- From scrum, to automation, to handful of people working around a machine, to punch cards (decomposing a larger system), collosis (loops,branches, etc)
- The 50s were how to manipulate and command a machine to get results; there was no 'software'
- We begin to see a set of people that can do the hardware vs the software
- Then we see the separation between programmers, punch cards, and the machine people
- The dichotomy of growth of software space vs hardware face
- IBM share was the first open source movement
- Sage was the largest software project ever
- Software unto itself had monetary value; later in the 60s
- The premise we made in the woven in loom of sorrow came from defense systems; what we learned from WWI and WWII
- The soviet union was moving in a way that made Sage utterly obsolete
- Moving to the business side of the world
- It was IBM that was the facebook/google/etc of the time
- Un-bundling of software vs hardware; this is where structures methods came to play
- This is where the waterfall method came to fruition
- Margaret declared the word software engineering before anyone else; in internal documents within Nasa
- Software was now looked at from an engineering point of view
- In engineering there is no such thing as a 'perfect system' there is only a good enough system
- In this time frame we begin to see the evolution of true software evolution methods
- Margaret began to realize there is an art/science to what we were doing at the time
- The revolution of the microprocessor allowed us to make our machines more personal !
- We then saw the growth of the variety of languages
- Algorithmic languages, then simula spss, etc
- We were starting to build mirror worlds!
- The things on the edge of our devices could have start to have intelligence in them as well
- The programming problem had shifted from being in the data center to being out on the edges
- We began to see the pendulum swinging back and forth of centralization and decentralization
- This changed the software equation again because now we have the notion of the predecessor of the app!
- We see visicalc - a small set of people could go off and write software independently from a set of hardware that other people supplied
- **There was massive economic value in this**
- Applications were still monolithic islands; the operating wars!
- We saw networks grow and grow until what we have today
- But there was another shift, now all of a sudden there is software that is mobile
- So how did this change the software equation?
- All of a sudden we were not building software for single machines, but for multiple machines, and for devices that were on the edge that have their own intelligence that leveraged those things on the edge
- Around this time we saw the language wars come to play
- the DOD wanted to reduce the languages
- SO Walah - the need to build simulations led us to the object-oriented paradigm
- Rather than having big monolithic phases; the industry discovered continuous evolution
- After arpanet - the birth of the internet of things!
- We could put our devices into the real world!
- The economic change happened in terms of the invention of the cloud, it came to be from pure economics - the cost of data centers
- Security becomes super relevant during this time period
- The shift we began to see was another economic one; dominant design, you will see a number of players go into the market-place and then consolidation around them
- Industry specific ecosystems; economically driven
- At Watson, you realized that you could refactor it for different services
- Now the engineering choice is what set of services to I build on
- The focus was no longer on the operating system wars but on the service wars- who controls the services into a particular ecosystem
- We tend to build really large systems - millions of lines of code
- There are people within these ecosystems who's job is to keep feeding the beast
- Keep building the infra - drawing people in
- Software development; by and large has broken itself up into three spaces; development that is happening behind the services (where C/C++/Java), You see a lot of stuff on the outside of the services (javascript and scripting languages tend to go here), and then the stuff that lives in the walls (in the hard real time space)
- Projecting forward: software is becoming merely intimate; becoming attached to us
- People are building systems that touch us intimately; dealing with different economics
- We're seeing a move to the edge again from the data centers
- The physics of data makes it more expensive to push data out to the edge
- We're begging to see greater intelligence in the edge
- The first computers were human
- Nueral Networks... the shift from deterministic systems to systems we teach and that learn
- This changes the lifecycle all together
- Training classifiers
- How do we build software cycles that are based on learning and evolve over time
- **SD in general are overlapping phases: Discovery, Invention, Execution**
- Focus on good abstractions, have a clear separation of concerns, and have punctuated equilibrium (have deliver of demonstrable things over time)
- Below the surface, on top of the surface, and outside
- The degree of formality; different stakeholders == different formalities
- Consider risks.. Scale...
- **The basic premise of science says we can look at the world, observe it, and reduce it to a set of understandable and fundamental principles**
- **software is the hidden writing that whispers the stories of possibilities to our hardware**
- The exciting thing is WE ARE THE STORY TELLERS

*End of Personal NOTES*

# Writing/Rubric for Semester readings and videos

# Writing for 'No Silver Bullet'

Title: "No Silver Bullet - Essence and Accident in Software Engineering" Brooks, F.P., J. (1987). No Silver Bullet Essence and Accidents of Software Engineering. Computer, 20, 10 -19. doi: 10.1109/MC.1987.1663532
Keywords: Complexity; Accidental vs Essential, Conformity, Changeability, and Breakthroughs

First Paragraph:

The main points of the article, "No Silver Bullet - Essence and Accident in Software Engineering", is that there is no inherent silver bullet.
What this means is that there is no single development that promises improvement in productivity, reliability, and simplicity. In other words,
we cannot expect to see a two-fold gain every two years in software development, as there is in hardware development.

Second Paragraph:

The strengths of the paper are lying out the differences accidental complexity and essential complexity. Accidental complexity is explained as problems that engineers are able to fix.
Whereas essential complexity is caused by the problem that is in the process of being solved. The paper is overall very clear and well written as it gives examples of both.
There is really no weakness aside from the abstract, I wish it was a little longer and explained everything in more detail before digging into the paper.

# Writing for 'They Write the Right Stuff' Fishman, Charles. “They Write the Right Stuff.” Fast Company, Fast Company, 8 Sept. 2017, https://www.fastcompany.com/28121/they-write-right-stuff.

Title: "They Write the Right Stuff"

First Paragraph:

The main points of the article, "They Write the Right Stuff", is how software engineers write the right stuff meaning software with extremely high stakes and the process by which the software is written.
The "process" that was invented to writing the software is vital, the software is only as good as the plan to write it, the best teamwork is healthy rivalry, and it's not just about fixing mistakes
one must fix whatever permitted the mistake to begin with. Software is literally everything, whether it of lesser importance like a website or controls the lives of astronauts the process, problem solving,
and teamwork is what truly creates the best software with relentless execution.

Second Paragraph:

The strengths of the paper are explaining how software is literally everything, the importance of how well some software has to be written because it is in control of human life, and how
the process can be reduced to four simple propositions. The paper is packed with dramatic detail for good reasoning, from explaining software controlling shuttle systems to the people that write
the best software in the world. While reading the article, it's easy to visualize words and sentences because of this detail. The only weakness is, I wish it went into more detail on
the four propositions and gave examples of exact processes engineers used at Nasa to produce the best software.

# Writing for 'CHM Live | The History (and the Future) of Software' Booch, Grady, director. CHM Live | The History (and the Future) of Software. Youtube, 2016.

Title: "CHM Live | The History (and the Future) of Software"

First Paragraph:

The main points of the video, "CHM Live | The History (and the Future) of Software", were explaining the history of software and hardware since the 1950s where humans were the first computers.
The video goes on to talk about the evolution of software, the dichotomy and separation of hardware/software, and how systems are build through changing economics.
Additionally, the Grady Booch presents a common theme throughout the entire video that software development in general has overlapping phases: Discovery, Invention, Execution.
This was a great video to learn about the history, present, and future of how software processes are evolving from "telling the computer what to do" to "the computer can now learn".

Second Paragraph:

The strengths of this video are too many to count. Grady goes is able to explain complex topics with simplicity. He is able to flawlessly present the first computers,
to how software development and the ecosystem has evolved today. This video is vital to understanding where software development came from and how it got to where it is now.
The video literally names the first woman who coined the term "software engineering". The only weakness I can think of is maybe having a timeline to see where Grady is going
with the presentation. That way, we can easily reference the important points instead of having to scrabble down notes to find the main themes. 





