# 1

# Writing for 'No Silver Bullet'

Title: "No Silver Bullet - Essence and Accident in Software Engineering" Brooks, F.P., J. (1987). No Silver Bullet Essence and Accidents of Software Engineering. Computer, 20, 10 -19. doi: 10.1109/MC.1987.1663532
Keywords: Complexity; Accidental vs Essential, Conformity, Changeability, and Breakthroughs

First Paragraph:

The main points of the article, "No Silver Bullet - Essence and Accident in Software Engineering", is that there is no inherent silver bullet.
What this means is that there is no single development that promises improvement in productivity, reliability, and simplicity. In other words,
we cannot expect to see a two-fold gain every two years in software development, as there is in hardware development.

Second Paragraph:

The strengths of the paper are lying out the differences accidental complexity and essential complexity. Accidental complexity is explained as problems that engineers are able to fix.
Whereas essential complexity is caused by the problem that is in the process of being solved. The paper is overall very clear and well written as it gives examples of both.
There is really no weakness aside from the abstract, I wish it was a little longer and explained everything in more detail before digging into the paper.

# Writing for 'They Write the Right Stuff' Fishman, Charles. “They Write the Right Stuff.” Fast Company, Fast Company, 8 Sept. 2017, https://www.fastcompany.com/28121/they-write-right-stuff.

Title: "They Write the Right Stuff"

First Paragraph:

The main points of the article, "They Write the Right Stuff", is how software engineers write the right stuff meaning software with extremely high stakes and the process by which the software is written.
The "process" that was invented to writing the software is vital, the software is only as good as the plan to write it, the best teamwork is healthy rivalry, and it's not just about fixing mistakes
one must fix whatever permitted the mistake to begin with. Software is literally everything, whether it of lesser importance like a website or controls the lives of astronauts the process, problem solving,
and teamwork is what truly creates the best software with relentless execution.

Second Paragraph:

The strengths of the paper are explaining how software is literally everything, the importance of how well some software has to be written because it is in control of human life, and how
the process can be reduced to four simple propositions. The paper is packed with dramatic detail for good reasoning, from explaining software controlling shuttle systems to the people that write
the best software in the world. While reading the article, it's easy to visualize words and sentences because of this detail. The only weakness is, I wish it went into more detail on
the four propositions and gave examples of exact processes engineers used at Nasa to produce the best software.

# Writing for 'CHM Live | The History (and the Future) of Software' Booch, Grady, director. CHM Live | The History (and the Future) of Software. Youtube, 2016.

Title: "CHM Live | The History (and the Future) of Software"

First Paragraph:

The main points of the video, "CHM Live | The History (and the Future) of Software", were explaining the history of software and hardware since the 1950s where humans were the first computers.
The video goes on to talk about the evolution of software, the dichotomy and separation of hardware/software, and how systems are build through changing economics.
Additionally, the Grady Booch presents a common theme throughout the entire video that software development in general has overlapping phases: Discovery, Invention, Execution.
This was a great video to learn about the history, present, and future of how software processes are evolving from "telling the computer what to do" to "the computer can now learn".

Second Paragraph:

The strengths of this video are too many to count. Grady goes is able to explain complex topics with simplicity. He is able to flawlessly present the first computers,
to how software development and the ecosystem has evolved today. This video is vital to understanding where software development came from and how it got to where it is now.
The video literally names the first woman who coined the term "software engineering". The only weakness I can think of is maybe having a timeline to see where Grady is going
with the presentation. That way, we can easily reference the important points instead of having to scrabble down notes to find the main themes. 

# 2 

<https://gitlab.oit.duke.edu/mrc90/ft512-assignment1/-/tree/main>

# 3 

# Times for tasks and additional notes

* Personal Notes were taken during each task
* Cloning gitlab and installing intelliJ - 0 hr 10 mins
* Reading 'No Silver Bullet: Essence and Accident in Software Engineering' - 0 hr 20 mins
* Reading 'They Write the Right Stuff' - 0 hr 13 mins
* CHM Live | The History (and the Future) of Software - 1 hr 09 min 27 sec
* Write ups - 0hr 30 mins
* Getting started with Java - 0 hr 30 mins
* The Money Example - 4 hrs 30 mins + longer because I had to redo it... 

