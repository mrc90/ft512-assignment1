// Michael Cutro
// Assignment 1 - Money Example and TDD
public class Dollar extends Money {

    private String currency;
    Dollar(int amount, String currency) {
        super(amount, currency);
    }
    String currency() {
        return currency;
    }

    // stub implementation of times
    // value of the multiplier is 2 from our test case
    public Money times(int multiplier) {
        return new Money(amount * multiplier, currency);
    }

    static Money dollar(int amount) {
        return new Money(amount, "USD");
    }

}
