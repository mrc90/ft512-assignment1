class Franc extends Money  {

    private String currency;
    Franc(int amount, String currency) {
        super(amount, currency);
    }
    String currency() {
        return currency;
    }

    public Money times(int multiplier) {
        return new Money(amount * multiplier, currency);
    }

    static Money franc(int amount){
        return new Money(amount, "CHF");
    }

}